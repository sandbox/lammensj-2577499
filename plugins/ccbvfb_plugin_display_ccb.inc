<?php


class ccbvfb_plugin_display_ccb extends content_callback_plugin_display_content_callback {

  /**
   * Allows ccb views to put exposed filter forms in blocks
   *
   * @return bool
   */
  public function uses_exposed_form_in_block() {
    return TRUE;
  }

  /**
   * @inheritdoc
   */
  public function option_definition() {
    $options = parent::option_definition();

    $options['exposed_block_form_destination'] = [
      'default' => ''
    ];

    return $options;
  }

  /**
   * @inheritdoc
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    if ($form_state['section'] == 'exposed_block') {
      $form['ebfd_wrapper'] = [
        '#type' => 'fieldset',
        '#title' => t('Form destination'),
        '#states' => [
          'visible' => [
            ':input[name="exposed_block"]' => ['value' => 1]
          ]
        ]
      ];
      $form['ebfd_wrapper']['exposed_block_form_destination'] = [
        '#title' => t('Exposed block form destination'),
        '#type' => 'textfield',
        '#default_value' => $this->get_option('exposed_block_form_destination'),
        '#description' => t('Submit a path to where to redirect the form. This can either be an absolute path (http://google.com) or an internal one (node/14) or the alias. Tokens are supported.')
      ];
      if (module_exists('token')) {
        $form['ebfd_wrapper']['token_help'] = [
          '#theme' => 'token_tree',
          '#token_types' => ['node']
        ];
      }
      else {
        $form['ebfd_wrapper']['token_help'] = [
          '#markup' => '<p>' . t('Enable the <a href="@drupal-token">Token module</a> to view the available token browser.', array('@drupal-token' => 'http://drupal.org/project/token')) . '</p>'
        ];
      }
    }
  }

  /**
   * @inheritdoc
   */
  public function options_submit(&$form, &$form_state) {
    parent::options_submit($form, $form_state);

    if ($form_state['values']['exposed_block']) {
      $destination = $form_state['values']['exposed_block_form_destination'];
      $this->set_option('exposed_block_form_destination', $form_state['values']['exposed_block_form_destination']);
      $this->set_option('path', token_replace($destination));
    }
  }

  /**
   * @inheritdoc
   */
  public function get_path() {
    if ($this->get_option('exposed_block')) {
      return $this->get_option('path');
    }

    parent::get_path();
  }

  /**
   * Look for nodes where this content callback is used in a field.
   *
   * @param $display_id
   * @return array
   */
  private function findActiveCallbacks($display_id) {
    $query = db_select('field_config', 'fc');
    $query->leftJoin('field_config_instance', 'fci', 'fc.field_name = fci.field_name');
    $query
      ->fields('fci', ['field_name'])
      ->where('fc.type = :type', [':type' => 'content_callback']);
    $result = $query->execute();

    $activeCallbacks = [];
    while ($row = $result->fetch()) {
      $query = new EntityFieldQuery();
      $query
        ->entityCondition('entity_type', 'node')
        ->fieldCondition($row->field_name, 'cid', '%' . $display_id, 'LIKE');
      $nodes = $query->execute();

      if (!empty($nodes)) {
        foreach ($nodes['node'] as $nid => $node) {
          $node = entity_metadata_wrapper('node', $nid);
          $activeCallbacks[$nid] = $node->label();
        }
      }
    }

    return $activeCallbacks;
  }
}