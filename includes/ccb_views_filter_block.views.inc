<?php
/**
 * @file
 * ccb_views_filter_block.views.inc
 */

/**
 * Implements hook_views_plugins_alter().
 */
function ccb_views_filter_block_views_plugins_alter(&$plugins) {
  // Force the ccb display plugin to use our ccb display plugin
  $plugins['display']['content_callback']['handler'] = 'ccbvfb_plugin_display_ccb';
  $plugins['display']['content_callback']['path'] = drupal_get_path('module', 'ccb_views_filter_block') . '/plugins';
  $plugins['display']['content_callback']['file'] = 'ccbvfb_plugin_display_ccb.inc';
}